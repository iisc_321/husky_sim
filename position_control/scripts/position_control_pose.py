#!/usr/bin/env python

import rospy, tf
import geometry_msgs.msg
from math import *

class position_controller():

    def __init__(self):

        rospy.init_node('position_control',anonymous=True)

        self.max_vel_ = rospy.get_param("/max_vel",1.0)
        self.pos_gain_ = rospy.get_param("/pose_gain",1.5)
        self.yaw_gain_ = rospy.get_param("/yaw_gain",0.5)
        self.dist_thresh_ = rospy.get_param("/dist_thresh",0.1)

        #self.goal = [5,0]

        self.cmdmsg = geometry_msgs.msg.Twist()
        self.cmdpub = rospy.Publisher('cmd_vel',geometry_msgs.msg.Twist,queue_size = 10)

        rospy.Subscriber("gps_transform", geometry_msgs.msg.TransformStamped, self.position_callback)
        rospy.Subscriber("setpoint",geometry_msgs.msg.PoseStamped,self.setpoint_callback)

        rospy.spin()

    def position_callback(self,msg):

        pos = msg.transform.translation
        quat = msg.transform.rotation

        angles = tf.transformations.euler_from_quaternion((quat.x,quat.y,
                                                           quat.z,quat.w))
        current_yaw = angles[2]

        pose = [pos.x, pos.y, current_yaw]  # X, Y, Theta

        v = 0 #default linear velocity
        r = 0 #default linear velocity

        distance = sqrt((pose[0]-self.goal[0])**2+(pose[1]-self.goal[1])**2)

        if distance > self.dist_thresh_:
            v = (distance-self.dist_thresh_) * self.pos_gain_
            v = min(self.max_vel_, max(-1 * self.max_vel_, v))
            des_yaw = atan2(self.goal[1]-pose[1],self.goal[0]-pose[0])
            u = des_yaw - current_yaw
            bound = atan2(sin(u),cos(u))
            r = min(0.5 , max(-0.5, self.yaw_gain_*bound))

        self.cmdmsg.linear.x = v
        self.cmdmsg.angular.z = r
        # self.cmdmsg.header.stamp = rospy.Time.now()
        self.cmdpub.publish(self.cmdmsg)

    def setpoint_callback(self,msg):
        self.goal = [msg.pose.position.x,msg.pose.position.y]

        # print('in call back')



if __name__ == '__main__':

    pos_controller = position_controller()
    # loop_rate = rospy.Rate(100)

